# InVEST Crop Production Model Global Dataset

Last Date Modified: 01/09/2021

## Contents
- climate_percentile_yield_tables (percentile model): For each crop, a CSV listing the 25th, 50th, 75th, and 95th percentile yields in each climate bin. These percentiles are derived from the global observed yield and climate bin datasets; for example, the 95th percentile value for wheat in climate bin 1 is 3.763889. This means that 95% of areas that grow wheat in climate bin 1 produce less than 3.763889 tons/hectare.
- climate_regression_yield_tables (regression model): For each crop, a CSV of regression parameters for each climate bin.
- crop_nutrient.csv (percentile and regression models): A table showing the nutritional values for each crop.
- extended_climate_bin_maps (percentile model): For each crop, a global raster of climate bins for that crop (see the Supplementary Methods of Mueller et al. 2012 for details).
- observed_yield (percentile model): For each crop, a global raster of actual observed yield circa the year 2000.

